## mindento.com - rozwiązanie zadania rekrutacyjnego - Robert Przybylski (robert.przybylski.95@gmail.com)

## Informacje
- PHP ver 8.2
- Symfony ver 6.3 (w podstawowej konfiguracji)
- baza danych - PostgreSQL ver 15
- środowisko uruchomieniowe Docker (Docker Compose v2.10+) (podstawowa konfiguracja dostarczona przez libkę https://github.com/dunglas/symfony-docker)

## Uruchomienie
1. `docker compose build --no-cache` - budowanie obrazu
2. `docker compose up  -d --wait` - uruchomienie kontenerów
3. Aplikacja dostępna pod `http://localhost` 
4.  `docker compose down --remove-orphans` - zatrzymanie kontenerów

## Testy
1. `composer tests:unit` - unit testy (znajdują się w `src/<nazwa_modulu>/Tests/Unit`)
2. `composer tests:integration` - testy integracyjne (znajdują się w `src/<nazwa_modulu>/Tests/Integration`)
3. `composer tests:http` - testy http (znajdują się w `src/<nazwa_modulu>/Tests/Http`)
4. `composer tests` - odpala wszystkie powyższe testy

## Moduły
1. `Delegation` - odpowiedzialny za dane dotyczące delegacji pracowników oraz wyliczanie stawek za delegację
2. `Employee` - odpowiedzialny za dane pracowników
2. `SharedKernel` - moduł do przechowywania wspólnych elementów

## API
1. API dostępne jest pod adresem http://localhost/api
2. Kolekcja postmanowa jest w `docs/postman`

## Co jeszcze można było zrobić?
1. Uuid-y agregatów wydzielić do osobnych ValueObject-ów np: `EmployeeeId`
2. Utworzyć moduł `Pricing`, odpowiedzialny za dane o cennikach, a także wydzielić w nim samo "wyliczanie" do osobnej subdomeny
3. Utworzyć moduł `Company`, który bylby odpowiedzialny za dane firmy
4. Dodać dokumentację API np: w Swaggerze

