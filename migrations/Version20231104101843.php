<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231104101843 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE delegation__delegation (delegation_uuid UUID NOT NULL, employee_uuid UUID NOT NULL, country VARCHAR(10) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, time_range_start_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, time_range_end_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, due_amount NUMERIC(12, 2) NOT NULL, due_currency VARCHAR(10) NOT NULL, PRIMARY KEY(delegation_uuid))');
        $this->addSql('COMMENT ON COLUMN delegation__delegation.delegation_uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN delegation__delegation.employee_uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN delegation__delegation.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN delegation__delegation.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN delegation__delegation.time_range_start_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN delegation__delegation.time_range_end_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE employee__employee (employee_uuid UUID NOT NULL, PRIMARY KEY(employee_uuid))');
        $this->addSql('COMMENT ON COLUMN employee__employee.employee_uuid IS \'(DC2Type:uuid)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE delegation__delegation');
        $this->addSql('DROP TABLE employee__employee');
    }
}
