<?php

declare(strict_types=1);

namespace App\Delegation\Infrastructure\Query;

use App\Delegation\Application\Query\EmployeeQueryInterface;
use App\Delegation\Application\Query\EmployeeView;
use App\Employee\Application\EmployeeFacade;

class ModuleEmployeeQuery implements EmployeeQueryInterface
{
    public function __construct(
        private readonly EmployeeFacade $employeeFacade
    ) {
    }

    public function findByUuid(string $employeeUuid): ?EmployeeView
    {
        $employee = $this->employeeFacade->getEmployee($employeeUuid);

        return null !== $employee
            ? EmployeeView::create($employee->employeeUuid)
            : null;
    }
}