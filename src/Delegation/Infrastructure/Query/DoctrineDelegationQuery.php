<?php

declare(strict_types=1);

namespace App\Delegation\Infrastructure\Query;

use App\Delegation\Application\Query\DelegationQueryInterface;
use App\Delegation\Application\Query\DelegationView;
use Doctrine\DBAL\Connection;
use Symfony\Component\Uid\Uuid;

class DoctrineDelegationQuery implements DelegationQueryInterface
{
    public const TABLE_NAME = 'delegation__delegation';

    public function __construct(
        private readonly Connection $connection
    ) {
    }

    public function findBy(array $params): array
    {
        $queryBuilder = $this->connection->createQueryBuilder()
            ->select('d.*')
            ->from(self::TABLE_NAME, 'd')
            ->addOrderBy('d.time_range_start_at', 'ASC');

        if (isset($params['employeeUuid'])) {
            $queryBuilder->andWhere('d.employee_uuid = :employeeUuid')
                ->setParameter('employeeUuid', $params['employeeUuid']);
        }

        $rows = $this->connection->fetchAllAssociative($queryBuilder->getSQL(), $queryBuilder->getParameters());

        $result = [];
        foreach ($rows as $row) {
            $result[] = DelegationView::deserialize($row);
        }

        return $result;
    }

    public function isExistDelegationForEmployeeInTime(
        Uuid $employeeUuid,
        \DateTimeImmutable $startAt,
        \DateTimeImmutable $endAt,
    ): bool {
        $queryBuilder = $this->connection->createQueryBuilder()
            ->select('d.delegation_uuid')
            ->from(self::TABLE_NAME, 'd')
            ->andWhere('d.employee_uuid = :employeeUuid')
            ->andWhere('d.time_range_start_at <= :endAt')
            ->andWhere('d.time_range_end_at >= :startAt')
            ->setParameter('employeeUuid', $employeeUuid)
            ->setParameter('startAt', $startAt->format('Y-m-d H:i:s'))
            ->setParameter('endAt', $endAt->format('Y-m-d H:i:s'))
            ->setMaxResults(1);

        $row = $this->connection->fetchOne($queryBuilder->getSQL(), $queryBuilder->getParameters());

        return false !== $row;
    }
}
