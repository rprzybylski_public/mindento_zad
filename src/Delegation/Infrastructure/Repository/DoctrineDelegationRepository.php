<?php

declare(strict_types=1);

namespace App\Delegation\Infrastructure\Repository;

use App\Delegation\Domain\Delegation;
use App\Delegation\Domain\DelegationRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

class DoctrineDelegationRepository implements DelegationRepositoryInterface
{
    public function __construct(
        private readonly EntityManagerInterface $em
    ) {
    }

    public function add(Delegation $delegation): void
    {
        $this->em->persist($delegation);
        $this->em->flush();
    }

    public function find(string $delegationUuid): ?Delegation
    {
        return $this->em->getRepository(Delegation::class)->find($delegationUuid);
    }
}
