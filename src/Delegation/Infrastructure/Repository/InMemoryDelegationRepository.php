<?php

declare(strict_types=1);

namespace App\Delegation\Infrastructure\Repository;

use App\Delegation\Domain\Delegation;
use App\Delegation\Domain\DelegationRepositoryInterface;

class InMemoryDelegationRepository implements DelegationRepositoryInterface
{
    private array $delegations = [];

    public function add(Delegation $delegation): void
    {
        $this->delegations[(string) $delegation->getDelegationUuid()] = $delegation;
    }

    public function find(string $delegationUuid): ?Delegation
    {
        return $this->delegations[$delegationUuid];
    }
}
