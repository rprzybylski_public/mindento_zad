<?php

declare(strict_types=1);

namespace App\Delegation\Domain\Policy;

use App\Delegation\Domain\DelegationTimeRange;

class DefaultDelegationDuePolicy
{
    public function paidDaysCount(
        DelegationTimeRange $delegationTimeRange,
        int $limitDays = 0,
        int $offsetDays = 0,
    ): int {
        $paidDays = 0;
        $firstDayToCalc = $this->getFirstDayToCalc($delegationTimeRange, $offsetDays);
        $lastDayToCalc = $this->getLastDayToCalc($delegationTimeRange, $offsetDays, $limitDays);

        if ($firstDayToCalc >= $lastDayToCalc) {
            return 0;
        }

        $currentDate = $firstDayToCalc;
        while ($currentDate <= $lastDayToCalc) {
            if (!$this->isWeekend($currentDate)
                && $this->areEnoughHours($currentDate, $lastDayToCalc)
            ) {
                $paidDays++;
            }

            $currentDate = $currentDate->modify('+1 day')->setTime(0, 0);
        }

        return $paidDays;
    }


    private function getFirstDayToCalc(DelegationTimeRange $delegationTimeRange, int $offsetDays): \DateTimeImmutable
    {
        return 0 < $offsetDays
            ? $delegationTimeRange->startAt()->modify(sprintf('+%d days', $offsetDays))->setTime(0, 0)
            : $delegationTimeRange->startAt();
    }

    private function getLastDayToCalc(DelegationTimeRange $delegationTimeRange, int $offsetDays, ?int $limitDays): \DateTimeImmutable
    {
        if (1 > $limitDays) {
            return $delegationTimeRange->endAt();
        }

        $offset = $offsetDays + $limitDays;
        if (0 === $offset) {
            return $delegationTimeRange->endAt();
        }

        $lastDayToCalc = $delegationTimeRange->startAt()->modify(sprintf('+%d days', $offset))->setTime(23, 59, 59);

        return min($lastDayToCalc, $delegationTimeRange->endAt());
    }

    private function isWeekend(\DateTimeImmutable $currentDate): bool
    {
        return (int) $currentDate->format('N') > 5;
    }

    private function areEnoughHours(
        \DateTimeImmutable $currentDate,
        \DateTimeImmutable $lastDayToCalc,
    ): bool {
        $endOfWorkingDay = $this->isTheSameDay($currentDate, $lastDayToCalc)
            ? $lastDayToCalc
            : $currentDate->modify('+1 day')->setTime(00, 00, 00);

        return 8 <= $this->fullHoursBetweenDateTimes($currentDate, $endOfWorkingDay);
    }

    private function isTheSameDay(
        \DateTimeImmutable $day1,
        \DateTimeImmutable $day2
    ): bool {
        return $day1->format('Y-m-d') === $day2->format('Y-m-d');
    }

    private function fullHoursBetweenDateTimes(
        \DateTimeImmutable $startAt,
        \DateTimeImmutable $endAt,
    ): int {
        $seconds = $endAt->getTimestamp() - $startAt->getTimestamp();
        $hours = floor($seconds / 3600);

        return (int) $hours;
    }
}
