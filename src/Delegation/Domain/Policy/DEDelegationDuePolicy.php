<?php

declare(strict_types=1);

namespace App\Delegation\Domain\Policy;

use App\Delegation\Domain\DelegationDuePolicyInterface;
use App\Delegation\Domain\DelegationTimeRange;
use App\SharedKernel\Domain\Currency;
use App\SharedKernel\Domain\Money;

class DEDelegationDuePolicy implements DelegationDuePolicyInterface
{
    private const DAYS_IN_FIRST_RATE = 7;
    private const DUE_PER_DAY_FIRST_RATE = 50;
    private const DUE_PER_DAY_SECOND_RATE = 100;

    public function __construct(
        private readonly DefaultDelegationDuePolicy $defaultDelegationDuePolicy
    ) {
    }

    public function calcDue(DelegationTimeRange $delegationTimeRange): Money
    {
        $dueInFirstRate = $this->getDueForFirstRate($delegationTimeRange);
        $dueInSecondRate = $this->getDueForSecondRate($delegationTimeRange);

        return $dueInFirstRate->add($dueInSecondRate);
    }

    private function getDueForFirstRate(DelegationTimeRange $delegationTimeRange): Money
    {
        $paidDays = $this->defaultDelegationDuePolicy->paidDaysCount($delegationTimeRange, self::DAYS_IN_FIRST_RATE - 1);

        return Money::create(self::DUE_PER_DAY_FIRST_RATE * $paidDays, Currency::PLN);
    }

    private function getDueForSecondRate(DelegationTimeRange $delegationTimeRange): Money
    {
        $paidDays = $this->defaultDelegationDuePolicy->paidDaysCount($delegationTimeRange, 0, self::DAYS_IN_FIRST_RATE);

        return Money::create(self::DUE_PER_DAY_SECOND_RATE * $paidDays, Currency::PLN);
    }
}
