<?php

declare(strict_types=1);

namespace App\Delegation\Domain;

interface DelegationRepositoryInterface
{
    public function add(Delegation $delegation): void;

    public function find(string $delegationUuid): ?Delegation;
}
