<?php

declare(strict_types=1);

namespace App\Delegation\Domain;

use App\Delegation\Domain\Exception\DelegationCanNotBeCreated;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Embeddable]
class DelegationTimeRange
{
    private function __construct(
        #[ORM\Column(type: 'datetime_immutable')]
        private \DateTimeImmutable $startAt,

        #[ORM\Column(type: 'datetime_immutable')]
        private \DateTimeImmutable $endAt,
    ) {
    }

    public static function create(
        \DateTimeImmutable $startAt,
        \DateTimeImmutable $endAt,
    ): self {
        if ($startAt > $endAt) {
            throw DelegationCanNotBeCreated::becauseStartAtIsAfterEndAt($startAt, $endAt);
        }

        return new self($startAt, $endAt);
    }

    public function startAt(): \DateTimeImmutable
    {
        return $this->startAt;
    }

    public function endAt(): \DateTimeImmutable
    {
        return $this->endAt;
    }
}
