<?php

declare(strict_types=1);

namespace App\Delegation\Domain;

use App\SharedKernel\Domain\Money;

interface DelegationDuePolicyInterface
{
    public function calcDue(DelegationTimeRange $delegationTimeRange): Money;
}
