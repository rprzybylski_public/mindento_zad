<?php

declare(strict_types=1);

namespace App\Delegation\Domain\Exception;

class DelegationCanNotBeCreated extends \Exception
{
    public static function becauseStartAtIsAfterEndAt(
        \DateTimeImmutable $startAt,
        \DateTimeImmutable $endAt,
    ): self {
        return new self(sprintf(
            'Delegation can not be created because can not start (%s) after end (%s)',
            $startAt->format('Y-m-d H:i:s'),
            $endAt->format('Y-m-d H:i:s'),
        ));
    }
}
