<?php

declare(strict_types=1);

namespace App\Delegation\Domain;

use App\SharedKernel\Domain\Country;
use App\SharedKernel\Domain\Money;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity]
#[ORM\Table(name: 'delegation__delegation')]
class Delegation
{
    private function __construct(
        #[ORM\Id]
        #[ORM\Column(type: 'uuid')]
        private readonly Uuid $delegationUuid,

        #[ORM\Column(type: 'uuid')]
        private readonly Uuid $employeeUuid,

        #[ORM\Embedded(class: DelegationTimeRange::class, columnPrefix: 'time_range_')]
        private DelegationTimeRange $timeRange,

        #[ORM\Column(type: 'string', length: 10, enumType: Country::class)]
        private Country $country,

        #[ORM\Embedded(class: Money::class, columnPrefix: 'due_')]
        private Money $due,

        #[ORM\Column(type: 'datetime_immutable')]
        private readonly \DateTimeImmutable $createdAt,

        #[ORM\Column(type: 'datetime_immutable', nullable: true)]
        private ?\DateTimeImmutable $updatedAt,
    ) {
    }

    public static function create(
        Uuid $delegationUuid,
        Uuid $employeeUuid,
        DelegationTimeRange $delegationTimeRange,
        Country $country,
        DelegationDuePolicyInterface $delegationDuePolicy
    ): self {
        return new self(
            $delegationUuid,
            $employeeUuid,
            $delegationTimeRange,
            $country,
            $delegationDuePolicy->calcDue($delegationTimeRange),
            new \DateTimeImmutable(),
            null,
        );
    }

    public function getDelegationUuid(): Uuid
    {
        return $this->delegationUuid;
    }
}
