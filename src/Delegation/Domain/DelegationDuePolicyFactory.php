<?php

declare(strict_types=1);

namespace App\Delegation\Domain;

use App\Delegation\Domain\Policy\DEDelegationDuePolicy;
use App\Delegation\Domain\Policy\DefaultDelegationDuePolicy;
use App\Delegation\Domain\Policy\GBDelegationDuePolicy;
use App\Delegation\Domain\Policy\PLDelegationDuePolicy;
use App\SharedKernel\Domain\Country;

class DelegationDuePolicyFactory
{
    public function __construct(
        private readonly DefaultDelegationDuePolicy $defaultDelegationDuePolicy
    ) {
    }

    public function create(Country $country): DelegationDuePolicyInterface
    {
        return match ($country) {
            Country::DE => new DEDelegationDuePolicy($this->defaultDelegationDuePolicy),
            Country::GB => new GBDelegationDuePolicy($this->defaultDelegationDuePolicy),
            Country::PL => new PLDelegationDuePolicy($this->defaultDelegationDuePolicy),
        };
    }
}
