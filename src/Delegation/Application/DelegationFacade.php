<?php

declare(strict_types=1);

namespace App\Delegation\Application;

use App\Delegation\Application\Command\CreateDelegationCommand;
use App\Delegation\Application\Query\DelegationQueryInterface;
use App\Delegation\Application\Query\DelegationView;
use Symfony\Component\Messenger\MessageBusInterface;

class DelegationFacade
{
    public function __construct(
        private readonly MessageBusInterface $commandBus,
        private readonly DelegationQueryInterface $delegationQuery,
    ) {
    }

    public function createDelegation(CreateDelegationCommand $command): void
    {
        $this->commandBus->dispatch($command);
    }

    /**
     * @return DelegationView[]
     */
    public function findDelegationsBy(array $params): array
    {
        return $this->delegationQuery->findBy($params);
    }
}
