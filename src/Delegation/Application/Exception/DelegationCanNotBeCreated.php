<?php

declare(strict_types=1);

namespace App\Delegation\Application\Exception;

class DelegationCanNotBeCreated extends \Exception
{
    public static function becauseEmployeeNotExist(string $employeeUuid): self
    {
        return new self(sprintf('Delegation can not be created because employee %s not exist', $employeeUuid));
    }

    public static function becauseEmployeeHasAnotherDelegationInThisTime(string $employeeUuid): self
    {
        return new self(sprintf('Delegation can not be created because employee %s has delegation in this time', $employeeUuid));
    }

    public static function becauseCountryIsNotSupported(string $country): self
    {
        return new self(sprintf('Delegation can not be created because country %s is not supported', $country));
    }
}
