<?php

declare(strict_types=1);

namespace App\Delegation\Application\Query;

use Symfony\Component\Uid\Uuid;

interface DelegationQueryInterface
{
    /**
     * @return DelegationView[]
     */
    public function findBy(array $params): array;

    public function isExistDelegationForEmployeeInTime(
        Uuid $employeeUuid,
        \DateTimeImmutable $startAt,
        \DateTimeImmutable $endAt,
    ): bool;
}
