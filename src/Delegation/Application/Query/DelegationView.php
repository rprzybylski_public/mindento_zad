<?php

declare(strict_types=1);

namespace App\Delegation\Application\Query;

class DelegationView implements \JsonSerializable
{
    private function __construct(
        public readonly string $start,
        public readonly string $end,
        public readonly string $country,
        public readonly float $amountDue,
        public readonly string $currency,
    ) {
    }

    public function serialize(): array
    {
        return [
            'start' => $this->start,
            'end' => $this->end,
            'country' => $this->country,
            'amount_due' => $this->amountDue,
            'currency' => $this->currency,
        ];
    }

    public function jsonSerialize(): array
    {
        return $this->serialize();
    }

    public static function deserialize(array $data): self
    {
        return new self(
            $data['time_range_start_at'],
            $data['time_range_end_at'],
            $data['country'],
            (float) $data['due_amount'],
            $data['due_currency'],
        );
    }
}
