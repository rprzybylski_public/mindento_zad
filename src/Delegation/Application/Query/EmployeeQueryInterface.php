<?php

declare(strict_types=1);

namespace App\Delegation\Application\Query;

interface EmployeeQueryInterface
{
    public function findByUuid(string $employeeUuid): ?EmployeeView;
}
