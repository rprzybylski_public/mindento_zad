<?php

declare(strict_types=1);

namespace App\Delegation\Application\CommandHandler;

use App\Delegation\Application\Command\CreateDelegationCommand;
use App\Delegation\Application\Exception\DelegationCanNotBeCreated;
use App\Delegation\Application\Query\DelegationQueryInterface;
use App\Delegation\Application\Query\EmployeeQueryInterface;
use App\Delegation\Domain\Delegation;
use App\Delegation\Domain\DelegationDuePolicyFactory;
use App\Delegation\Domain\DelegationRepositoryInterface;
use App\Delegation\Domain\DelegationTimeRange;
use App\SharedKernel\Domain\Country;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Uid\Uuid;

#[AsMessageHandler]
class CreateDelegationCommandHandler
{
    public function __construct(
        private readonly DelegationRepositoryInterface $delegationRepository,
        private readonly DelegationDuePolicyFactory $delegationDuePolicyFactory,
        private readonly EmployeeQueryInterface $employeeQuery,
        private readonly DelegationQueryInterface $delegationQuery,
    ) {
    }

    public function __invoke(CreateDelegationCommand $command): void
    {
        if (!$this->isEmployeeExist($command)) {
            throw DelegationCanNotBeCreated::becauseEmployeeNotExist($command->employeeUuid);
        }

        if ($this->hasEmployeeAnotherDelegationInThisTime($command)) {
            throw DelegationCanNotBeCreated::becauseEmployeeHasAnotherDelegationInThisTime($command->employeeUuid);
        }

        $country = Country::tryfrom($command->country);
        if (null === $country) {
            throw DelegationCanNotBeCreated::becauseCountryIsNotSupported($command->country);
        }

        $delegation = Delegation::create(
            Uuid::fromString($command->delegationUuid),
            Uuid::fromString($command->employeeUuid),
            DelegationTimeRange::create(
                new \DateTimeImmutable($command->startAt),
                new \DateTimeImmutable($command->endAt),
            ),
            $country,
            $this->delegationDuePolicyFactory->create($country),
        );

        $this->delegationRepository->add($delegation);
    }

    private function isEmployeeExist(CreateDelegationCommand $command): bool
    {
        return null !== $this->employeeQuery->findByUuid($command->employeeUuid);
    }

    private function hasEmployeeAnotherDelegationInThisTime(CreateDelegationCommand $command): bool
    {
        return $this->delegationQuery->isExistDelegationForEmployeeInTime(
            Uuid::fromString($command->employeeUuid),
            new \DateTimeImmutable($command->startAt),
            new \DateTimeImmutable($command->endAt),
        );
    }
}
