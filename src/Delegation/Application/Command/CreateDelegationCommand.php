<?php

declare(strict_types=1);

namespace App\Delegation\Application\Command;

class CreateDelegationCommand
{
    public function __construct(
        public readonly string $delegationUuid,
        public readonly string $employeeUuid,
        public readonly string $startAt,
        public readonly string $endAt,
        public readonly string $country,
    ) {
    }
}
