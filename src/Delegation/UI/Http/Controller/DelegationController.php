<?php

declare(strict_types=1);

namespace App\Delegation\UI\Http\Controller;

use App\Delegation\Application\Command\CreateDelegationCommand;
use App\Delegation\Application\DelegationFacade;
use App\Delegation\UI\Http\Request\CreateDelegationRequest;
use App\Delegation\UI\Http\Request\GetDelegationsCollectionRequest;
use App\SharedKernel\UI\Http\RequestValidator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;

class DelegationController extends AbstractController
{
    public function __construct(
        private readonly RequestValidator $requestValidator,
        private readonly DelegationFacade $delegationFacade,
    ) {
    }

    #[Route(
        '/api/delegation/delegation',
        name: 'app_delegation_create_delegation',
        methods: ['POST']
    )]
    public function createDelegation(CreateDelegationRequest $request): JsonResponse
    {
        $this->requestValidator->validate($request);

        $delegationUuid = (string) Uuid::v4();
        $this->delegationFacade->createDelegation(new CreateDelegationCommand(
            $delegationUuid,
            (string) $request->employeeUuid,
            (string) $request->startAt,
            (string) $request->endAt,
            (string) $request->country,
        ));

        return $this->json([
            'delegation_uuid' => $delegationUuid,
        ], Response::HTTP_CREATED);
    }

    #[Route(
        '/api/delegation/delegation',
        name: 'app_delegation_get_delegations_collection',
        methods: ['GET']
    )]
    public function getDelegationsCollection(GetDelegationsCollectionRequest $request): JsonResponse
    {
        $delegations = $this->delegationFacade->findDelegationsBy([
            'employeeUuid' => $request->employeeUuid,
        ]);

        return $this->json($delegations, Response::HTTP_OK);
    }
}
