<?php

declare(strict_types=1);

namespace App\Delegation\UI\Http\Request;

use App\SharedKernel\Domain\Country;
use App\SharedKernel\UI\Http\AppJsonRequest;
use Symfony\Component\Validator\Constraints as Assert;

class CreateDelegationRequest implements AppJsonRequest
{
    public function __construct(
        #[Assert\NotBlank]
        #[Assert\Uuid(strict: false)]
        public $employeeUuid = null,

        #[Assert\NotBlank]
        #[Assert\Type(type: 'string')]
        public $startAt = null,

        #[Assert\NotBlank]
        #[Assert\Type(type: 'string')]
        public $endAt = null,

        #[Assert\NotBlank]
        #[Assert\Type(type: 'string')]
        #[Assert\Country]
        public $country = null,
    ) {
    }

    #[Assert\IsTrue(message: 'start_at is not in correct format')]
    public function isStartAtInCorrectFormat(): bool
    {
        return false !== \DateTimeImmutable::createFromFormat('Y-m-d H:i:s', (string) $this->startAt);
    }

    #[Assert\IsTrue(message: 'end_at is not in correct format')]
    public function isEndAtInCorrectFormat(): bool
    {
        return false !== \DateTimeImmutable::createFromFormat('Y-m-d H:i:s', (string) $this->endAt);
    }

    #[Assert\IsTrue(message: 'Delegation can not start after end')]
    public function isEndAtAfterStartAt(): bool
    {
        if (!\DateTimeImmutable::createFromFormat('Y-m-d H:i:s', (string) $this->startAt)
            || !\DateTimeImmutable::createFromFormat('Y-m-d H:i:s', (string) $this->endAt)
        ) {
            return true;
        }

        return new \DateTimeImmutable((string) $this->endAt) >= new \DateTimeImmutable((string) $this->startAt);
    }

    #[Assert\IsTrue(message: 'Country is not supported')]
    public function isSupportedCountry(): bool
    {
        return null !== Country::tryFrom((string) $this->country);
    }
}
