<?php

declare(strict_types=1);

namespace App\Delegation\UI\Http\Request;

use App\SharedKernel\UI\Http\AppRequestWithQueryParameter;

class GetDelegationsCollectionRequest implements AppRequestWithQueryParameter
{
    public function __construct(
        public readonly string $employeeUuid,
    ) {
    }
}
