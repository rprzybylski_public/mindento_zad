<?php

declare(strict_types=1);

namespace App\Delegation\Tests\Http;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class CreateDelegationTest extends WebTestCase
{
    public function testCreateDelegation(): void
    {
        $client = static::createClient();

        $date = [
            'employeeUuid' => '11111111-1111-1111-1111-111111111111',
            'startAt' => '2023-02-01 08:00:00',
            'endAt' => '2023-02-02 16:00:00',
            'country' => 'PL',
        ];

        $client->request('POST', '/api/delegation/delegation', [], [], [], json_encode($date));

        self::assertResponseStatusCodeSame(Response::HTTP_CREATED);
    }
}
