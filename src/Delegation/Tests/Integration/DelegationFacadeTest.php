<?php

declare(strict_types=1);

namespace App\Delegation\Tests\Integration;

use App\Delegation\Application\Command\CreateDelegationCommand;
use App\Delegation\Application\DelegationFacade;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Uid\Uuid;

class DelegationFacadeTest extends KernelTestCase
{
    public function testDelegationSuccessFacade(): void
    {
        self::bootKernel();

        // given
        /** @var DelegationFacade $facade */
        $facade = self::getContainer()->get(DelegationFacade::class);
        $delegationUuid = Uuid::v4();
        $employeeUuid = Uuid::fromString('11111111-1111-1111-1111-111111111111');
        $startAt = '2023-01-02 08:00:00';
        $endAt = '2023-01-03 16:00:00';
        $country = 'PL';

        // when
        $facade->createDelegation(new CreateDelegationCommand(
            (string) $delegationUuid,
            (string) $employeeUuid,
            $startAt,
            $endAt,
            $country
        ));
        $delegations = $facade->findDelegationsBy([
            'employeeUuid' => (string) $employeeUuid
        ]);

        // then
        self::assertNotEmpty($delegations);
    }
}
