<?php

declare(strict_types=1);

namespace App\Delegation\Tests\Unit;

use App\Delegation\Application\Query\DelegationQueryInterface;
use App\Delegation\Application\Query\EmployeeQueryInterface;
use App\Delegation\Application\Query\EmployeeView;
use App\Delegation\Domain\DelegationDuePolicyFactory;
use App\Delegation\Domain\Policy\DefaultDelegationDuePolicy;
use App\Delegation\Infrastructure\Repository\InMemoryDelegationRepository;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Uid\Uuid;

abstract class TestUnitBase extends TestCase
{
    protected InMemoryDelegationRepository $inMemoryDelegationRepository;
    protected DelegationDuePolicyFactory $delegationDuePolicyFactory;

    protected function setUp(): void
    {
        $this->inMemoryDelegationRepository = new InMemoryDelegationRepository();
        $this->delegationDuePolicyFactory = new DelegationDuePolicyFactory(
            new DefaultDelegationDuePolicy()
        );

        parent::setUp();
    }

    protected function employeeQueryReturnEmployee(): EmployeeQueryInterface
    {
        $employeeQuery = $this->createStub(EmployeeQueryInterface::class);
        $employeeQuery
            ->method('findByUuid')
            ->willReturn(
                EmployeeView::create((string) Uuid::v4())
            );

        return $employeeQuery;
    }

    protected function employeeQueryReturnNull(): EmployeeQueryInterface
    {
        $employeeQuery = $this->createStub(EmployeeQueryInterface::class);
        $employeeQuery
            ->method('findByUuid')
            ->willReturn(null);

        return $employeeQuery;
    }

    protected function delegationQueryReturnTrue(): DelegationQueryInterface
    {
        $delegationQuery = $this->createStub(DelegationQueryInterface::class);
        $delegationQuery
            ->method('isExistDelegationForEmployeeInTime')
            ->willReturn(true);

        return $delegationQuery;
    }

    protected function delegationQueryReturnFalse(): DelegationQueryInterface
    {
        $delegationQuery = $this->createStub(DelegationQueryInterface::class);
        $delegationQuery
            ->method('isExistDelegationForEmployeeInTime')
            ->willReturn(false);

        return $delegationQuery;
    }
}
