<?php

declare(strict_types=1);

namespace App\Delegation\Tests\Unit\Policy;

use App\Delegation\Domain\DelegationTimeRange;
use App\Delegation\Domain\Policy\DEDelegationDuePolicy;
use App\Delegation\Domain\Policy\DefaultDelegationDuePolicy;
use App\Delegation\Tests\Unit\TestUnitBase;
use App\SharedKernel\Domain\Currency;
use App\SharedKernel\Domain\Money;

class DEDelegationDuePolicyTest extends TestUnitBase
{
    public static function calcDueDataProvider(): array
    {
        return [
            'one day delegation under 8 hours' => [
                '2023-01-02 08:00:00',
                '2023-01-02 10:00:00',
                Money::create(0, Currency::PLN),
            ],
            'one day delegation above 8 hours' => [
                '2023-01-02 08:00:00',
                '2023-01-02 18:00:00',
                Money::create(50, Currency::PLN),
            ],
            'five working day delegation' => [
                '2023-01-02 08:00:00',
                '2023-01-06 18:00:00',
                Money::create(250, Currency::PLN),
            ],
            'five day delegation with weekend' => [
                '2023-01-05 08:00:00',
                '2023-01-09 18:00:00',
                Money::create(150, Currency::PLN),
            ],
            'five working day delegation with first day under 8 hours' => [
                '2023-01-02 20:00:00',
                '2023-01-06 18:00:00',
                Money::create(200, Currency::PLN),
            ],
            'five working day delegation with last day under 8 hours' => [
                '2023-01-02 08:00:00',
                '2023-01-06 02:00:00',
                Money::create(200, Currency::PLN),
            ],
            'five working day delegation with first and last day under 8 hours' => [
                '2023-01-02 20:00:00',
                '2023-01-06 02:00:00',
                Money::create(150, Currency::PLN),
            ],
            'ten working day delegation with two weekends' => [
                '2023-01-02 08:00:00',
                '2023-01-15 16:00:00',
                Money::create(750, Currency::PLN),
            ],
        ];
    }

    /**
     * @dataProvider calcDueDataProvider
     */
    public function testCalcDueWorkCorrect(string $startAt, string $endAt, Money $expectedDue): void
    {
        //given
        $timeRange = DelegationTimeRange::create(new \DateTimeImmutable($startAt), new \DateTimeImmutable($endAt));

        $deDelegationDuePolicy = new DEDelegationDuePolicy(
            new DefaultDelegationDuePolicy()
        );

        // when
        $due = $deDelegationDuePolicy->calcDue($timeRange);

        // then
        self::assertTrue($expectedDue->isEqualTo($due));
    }
}
