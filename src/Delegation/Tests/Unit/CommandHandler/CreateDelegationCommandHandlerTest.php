<?php

declare(strict_types=1);

namespace App\Delegation\Tests\Unit\CommandHandler;

use App\Delegation\Application\Command\CreateDelegationCommand;
use App\Delegation\Application\CommandHandler\CreateDelegationCommandHandler;
use App\Delegation\Application\Exception\DelegationCanNotBeCreated;
use App\Delegation\Domain\Exception\DelegationCanNotBeCreated as DomainDelegationCanNotBeCreated;
use App\Delegation\Tests\Unit\TestUnitBase;
use Symfony\Component\Uid\Uuid;

class CreateDelegationCommandHandlerTest extends TestUnitBase
{
    public function testCreateDelegationSuccess(): void
    {
        // given
        $delegationUuid = (string) Uuid::v4();
        $employeeUuid = (string) Uuid::v4();
        $startAt = '2023-01-01 00:00:00';
        $endAt = '2023-01-02 00:00:00';
        $country = 'PL';

        $command = new CreateDelegationCommand(
            $delegationUuid,
            $employeeUuid,
            $startAt,
            $endAt,
            $country
        );
        $handler = new CreateDelegationCommandHandler(
            $this->inMemoryDelegationRepository,
            $this->delegationDuePolicyFactory,
            $this->employeeQueryReturnEmployee(),
            $this->delegationQueryReturnFalse(),
        );

        // when
        $handler->__invoke($command);
        $delegation = $this->inMemoryDelegationRepository->find($delegationUuid);

        // then
        self::assertNotNull($delegation);
    }

    public function testCreateDelegationFailedBecauseEmployeeNotExist(): void
    {
        //then
        $this->expectException(DelegationCanNotBeCreated::class);

        // given
        $delegationUuid = (string) Uuid::v4();
        $employeeUuid = (string) Uuid::v4();
        $startAt = '2023-01-01 00:00:00';
        $endAt = '2023-01-02 00:00:00';
        $country = 'PL';

        $command = new CreateDelegationCommand(
            $delegationUuid,
            $employeeUuid,
            $startAt,
            $endAt,
            $country
        );
        $handler = new CreateDelegationCommandHandler(
            $this->inMemoryDelegationRepository,
            $this->delegationDuePolicyFactory,
            $this->employeeQueryReturnNull(),
            $this->delegationQueryReturnFalse(),
        );

        // when
        $handler->__invoke($command);
    }

    public function testCreateDelegationFailedBecauseEmployeeHasAnotherDelegationInThisTime(): void
    {
        //then
        $this->expectException(DelegationCanNotBeCreated::class);

        // given
        $delegationUuid = (string) Uuid::v4();
        $employeeUuid = (string) Uuid::v4();
        $startAt = '2023-01-01 00:00:00';
        $endAt = '2023-01-02 00:00:00';
        $country = 'PL';

        $command = new CreateDelegationCommand(
            $delegationUuid,
            $employeeUuid,
            $startAt,
            $endAt,
            $country
        );
        $handler = new CreateDelegationCommandHandler(
            $this->inMemoryDelegationRepository,
            $this->delegationDuePolicyFactory,
            $this->employeeQueryReturnEmployee(),
            $this->delegationQueryReturnTrue(),
        );

        // when
        $handler->__invoke($command);
    }

    public function testCreateDelegationFailedBecauseCountryIsNotSupported(): void
    {
        $this->expectException(DelegationCanNotBeCreated::class);

        // given
        $delegationUuid = (string) Uuid::v4();
        $employeeUuid = (string) Uuid::v4();
        $startAt = '2023-01-01 00:00:00';
        $endAt = '2023-01-02 00:00:00';
        $country = 'NN';

        $command = new CreateDelegationCommand(
            $delegationUuid,
            $employeeUuid,
            $startAt,
            $endAt,
            $country
        );
        $handler = new CreateDelegationCommandHandler(
            $this->inMemoryDelegationRepository,
            $this->delegationDuePolicyFactory,
            $this->employeeQueryReturnNull(),
            $this->delegationQueryReturnFalse(),
        );

        // when
        $handler->__invoke($command);
    }

    public function testCreateDelegationFailedBecauseStartIsAfterEnd(): void
    {
        //then
        $this->expectException(DomainDelegationCanNotBeCreated::class);

        // given
        $delegationUuid = (string) Uuid::v4();
        $employeeUuid = (string) Uuid::v4();
        $startAt = '2023-01-02 00:00:00';
        $endAt = '2023-01-01 00:00:00';
        $country = 'PL';

        $command = new CreateDelegationCommand(
            $delegationUuid,
            $employeeUuid,
            $startAt,
            $endAt,
            $country
        );
        $handler = new CreateDelegationCommandHandler(
            $this->inMemoryDelegationRepository,
            $this->delegationDuePolicyFactory,
            $this->employeeQueryReturnEmployee(),
            $this->delegationQueryReturnFalse(),
        );

        // when
        $handler->__invoke($command);
    }
}
