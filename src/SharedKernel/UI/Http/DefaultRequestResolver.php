<?php

declare(strict_types=1);

namespace App\SharedKernel\UI\Http;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\SerializerInterface;

class DefaultRequestResolver implements ValueResolverInterface
{
    public function __construct(
        private readonly SerializerInterface $serializer
    ) {
    }

    public function resolve(Request $request, ArgumentMetadata $argument): array
    {
        $argumentType = $argument->getType();
        if (null === $argumentType) {
            return [];
        }

        if (is_subclass_of($argumentType, AppJsonRequest::class)) {
            return [
                $this->serializer->deserialize($request->getContent(), $argumentType, 'json'),
            ];
        }

        if (is_subclass_of($argumentType, AppRequestWithQueryParameter::class, true)) {
            return [
                $this->serializer->deserialize(json_encode($request->query->all()), $argumentType, 'json'),
            ];
        }

        return [];
    }
}
