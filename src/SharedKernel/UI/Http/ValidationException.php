<?php

declare(strict_types=1);

namespace App\SharedKernel\UI\Http;

use Symfony\Component\Validator\ConstraintViolationListInterface;

class ValidationException extends \Exception
{
    public function __construct(
        public readonly ConstraintViolationListInterface $violationList
    ) {
        parent::__construct();
    }
}
