<?php

declare(strict_types=1);

namespace App\SharedKernel\UI\Http;

use Symfony\Component\Validator\Validator\ValidatorInterface;

class RequestValidator
{
    public function __construct(
        private readonly ValidatorInterface $validator
    ) {
    }

    public function validate(object $request): void
    {
        $errors = $this->validator->validate($request);
        if (0 < $errors->count()) {
            throw new ValidationException($errors);
        }
    }
}
