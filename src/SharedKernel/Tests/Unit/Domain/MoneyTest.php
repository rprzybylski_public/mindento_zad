<?php

declare(strict_types=1);

namespace App\SharedKernel\Tests\Unit\Domain;

use App\SharedKernel\Domain\Currency;
use App\SharedKernel\Domain\Money;
use PHPUnit\Framework\TestCase;

class MoneyTest extends TestCase
{
    public function testCreateMoney(): void
    {
        // given
        $amount = 10.50;
        $currency = Currency::PLN;

        // when
        $money = Money::create($amount, $currency);

        // then
        $this->assertEquals(10.5, $money->amount());
        $this->assertEquals(Currency::PLN, $money->currency());
    }

    public function testAddMoney(): void
    {
        // given
        $money1 = Money::create(10.50, Currency::PLN);
        $money2 = Money::create(15.25, Currency::PLN);

        // when
        $sum = $money1->add($money2);

        // then
        $this->assertEquals(25.75, $sum->amount());
        $this->assertEquals(Currency::PLN, $sum->currency());
    }

    public function testIsEqualTo(): void
    {
        // given
        $money1 = Money::create(10.50, Currency::PLN);
        $money2 = Money::create(10.50, Currency::PLN);
        $money3 = Money::create(5.25, Currency::PLN);

        // when/then
        $this->assertTrue($money1->isEqualTo($money2));
        $this->assertFalse($money1->isEqualTo($money3));
    }
}
