<?php

declare(strict_types=1);

namespace App\SharedKernel\Infrastructure;

use App\SharedKernel\UI\Http\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class ApiExceptionListener
{
    public function __invoke(ExceptionEvent $event): void
    {
        if (!$event->isMainRequest()) {
            return;
        }

        $exception = $event->getThrowable();
        if ($exception instanceof ValidationException) {
            $content = [];
            foreach ($exception->violationList as $error) {
                $content[$error->getPropertyPath()] = $error->getMessage();
            }

            $event->setResponse(
                new JsonResponse($content, Response::HTTP_BAD_REQUEST)
            );
        } else {
            $content = [
                'message' => $exception->getPrevious()?->getMessage() ?? $exception->getMessage(),
            ];
        }

        $event->setResponse(
            new JsonResponse($content, Response::HTTP_BAD_REQUEST)
        );
    }
}
