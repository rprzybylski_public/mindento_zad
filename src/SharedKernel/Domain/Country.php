<?php

declare(strict_types=1);

namespace App\SharedKernel\Domain;

enum Country: string
{
    case PL = 'PL';
    case GB = 'GB';
    case DE = 'DE';

    public function isEqualTo(self $country): bool
    {
        return $this->value === $country->value();
    }

    public function value(): string
    {
        return $this->value;
    }
}
