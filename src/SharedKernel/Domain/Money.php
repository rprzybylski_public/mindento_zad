<?php

declare(strict_types=1);

namespace App\SharedKernel\Domain;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Embeddable]
class Money
{
    #[ORM\Column(type: 'decimal', precision: 12, scale: 2)]
    private float $amount;

    #[ORM\Column(type: 'string', length: 10, enumType: Currency::class)]
    private Currency $currency;

    private function __construct(float|int $amount, Currency $currency)
    {
        $this->amount = round($amount, 2);
        $this->currency = $currency;
    }

    public static function create(float|int $amount, Currency $currency): self
    {
        return new self($amount, $currency);
    }

    public function add(Money $money): Money
    {
        if ($this->currency !== $money->currency()) {
            throw new \DomainException('Operations with different currencies cannot be performed');
        }

        return new Money($this->amount + $money->amount(), $this->currency);
    }

    public function isEqualTo(Money $money): bool
    {
        return $this->amount === $money->amount() && $this->currency->isEqualTo($money->currency());
    }

    public function amount(): float
    {
        return $this->amount;
    }

    public function currency(): Currency
    {
        return $this->currency;
    }
}
