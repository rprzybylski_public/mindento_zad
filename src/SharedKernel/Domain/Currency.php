<?php

declare(strict_types=1);

namespace App\SharedKernel\Domain;

enum Currency: string
{
    case PLN = 'PLN';

    public function isEqualTo(self $currency): bool
    {
        return $this->value === $currency->value();
    }

    public function value(): string
    {
        return $this->value;
    }
}
