<?php

declare(strict_types=1);

namespace App\Employee\Tests\Integration;

use App\Employee\Application\Command\CreateEmployeeCommand;
use App\Employee\Application\EmployeeFacade;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Uid\Uuid;

class EmployeeFacadeTest extends KernelTestCase
{
    public function testEmployeeSuccessFacade(): void
    {
        self::bootKernel();

        // given
        /** @var EmployeeFacade $facade */
        $facade = self::getContainer()->get(EmployeeFacade::class);
        $employeeUuid = Uuid::v4();

        // when
        $facade->createEmployee(new CreateEmployeeCommand((string) $employeeUuid));
        $employee = $facade->getEmployee((string) $employeeUuid);

        // then
        self::assertNotNull($employee);
    }
}
