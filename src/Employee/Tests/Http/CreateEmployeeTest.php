<?php

declare(strict_types=1);

namespace App\Employee\Tests\Http;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class CreateEmployeeTest extends WebTestCase
{
    public function testCreateEmployee(): void
    {
        $client = static::createClient();

        $client->request('POST', '/api/employee/employee');

        self::assertResponseStatusCodeSame(Response::HTTP_CREATED);
    }
}