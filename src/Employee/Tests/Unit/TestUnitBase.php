<?php

declare(strict_types=1);

namespace App\Employee\Tests\Unit;

use App\Employee\Infrastructure\Repository\InMemoryEmployeeRepository;
use PHPUnit\Framework\TestCase;

abstract class TestUnitBase extends TestCase
{
    protected InMemoryEmployeeRepository $employeeRepository;

    protected function setUp(): void
    {
        $this->employeeRepository = new InMemoryEmployeeRepository();
        parent::setUp();
    }
}
