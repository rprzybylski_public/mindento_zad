<?php

declare(strict_types=1);

namespace App\Employee\Tests\Unit\CommandHandler;

use App\Employee\Application\Command\CreateEmployeeCommand;
use App\Employee\Application\CommandHandler\CreateEmployeeCommandHandler;
use App\Employee\Tests\Unit\TestUnitBase;
use Symfony\Component\Uid\Uuid;

class CreateEmployeeCommandHandlerTest extends TestUnitBase
{
    public function testCreateEmployeeSuccess(): void
    {
        // given
        $employeeUuid = (string) Uuid::v4();
        $command = new CreateEmployeeCommand($employeeUuid);
        $handler = new CreateEmployeeCommandHandler(
            $this->employeeRepository,
        );

        //when
        $handler->__invoke($command);
        $employee = $this->employeeRepository->find($employeeUuid);

        //then
        self::assertNotNull($employee);
    }
}