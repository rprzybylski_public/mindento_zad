<?php

declare(strict_types=1);

namespace App\Employee\Application\Query;

interface EmployeeQueryInterface
{
    public function findEmployee(string $employeeUuid): ?EmployeeView;
}
