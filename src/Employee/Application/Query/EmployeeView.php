<?php

declare(strict_types=1);

namespace App\Employee\Application\Query;

class EmployeeView implements \JsonSerializable
{
    private function __construct(
        public readonly string $employeeUuid,
    ) {
    }

    public function serialize(): array
    {
        return [
            'employee_uuid' => $this->employeeUuid,
        ];
    }

    public function jsonSerialize(): array
    {
        return $this->serialize();
    }

    public static function deserialize(array $data): self
    {
        return new self(
            $data['employee_uuid'],
        );
    }
}
