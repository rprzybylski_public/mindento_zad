<?php

declare(strict_types=1);

namespace App\Employee\Application\CommandHandler;

use App\Employee\Application\Command\CreateEmployeeCommand;
use App\Employee\Domain\Employee;
use App\Employee\Domain\EmployeeRepositoryInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Uid\Uuid;

#[AsMessageHandler]
class CreateEmployeeCommandHandler
{
    public function __construct(
        private readonly EmployeeRepositoryInterface $employeeRepository,
    ) {
    }

    public function __invoke(CreateEmployeeCommand $command): void
    {
        $employee = Employee::create(
            Uuid::fromString($command->employeeUuid),
        );

        $this->employeeRepository->add($employee);
    }
}
