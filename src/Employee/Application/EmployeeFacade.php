<?php

declare(strict_types=1);

namespace App\Employee\Application;

use App\Employee\Application\Command\CreateEmployeeCommand;
use App\Employee\Application\Query\EmployeeQueryInterface;
use App\Employee\Application\Query\EmployeeView;
use Symfony\Component\Messenger\MessageBusInterface;

class EmployeeFacade
{
    public function __construct(
        private readonly MessageBusInterface $commandBus,
        private readonly EmployeeQueryInterface $employeeQuery,
    ) {
    }

    public function createEmployee(CreateEmployeeCommand $command): void
    {
        $this->commandBus->dispatch($command);
    }

    public function getEmployee(string $employeeUuid): ?EmployeeView
    {
        return $this->employeeQuery->findEmployee($employeeUuid);
    }
}
