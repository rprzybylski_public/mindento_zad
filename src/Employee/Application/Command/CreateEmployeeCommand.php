<?php

declare(strict_types=1);

namespace App\Employee\Application\Command;

class CreateEmployeeCommand
{
    public function __construct(
        public readonly string $employeeUuid,
    ) {
    }
}
