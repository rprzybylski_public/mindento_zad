<?php

declare(strict_types=1);

namespace App\Employee\Domain;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity]
#[ORM\Table(name: 'employee__employee')]
class Employee
{
    private function __construct(
        #[ORM\Id]
        #[ORM\Column(type: 'uuid')]
        private readonly Uuid $employeeUuid
    ) {
    }

    public static function create(Uuid $employeeUuid): self
    {
        return new self($employeeUuid);
    }

    public function getEmployeeUuid(): Uuid
    {
        return $this->employeeUuid;
    }
}
