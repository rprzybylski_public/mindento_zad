<?php

declare(strict_types=1);

namespace App\Employee\Domain;

interface EmployeeRepositoryInterface
{
    public function add(Employee $employee): void;

    public function find(string $employeeUuid): ?Employee;
}
