<?php

declare(strict_types=1);

namespace App\Employee\UI\Http\Controller;

use App\Employee\Application\Command\CreateEmployeeCommand;
use App\Employee\Application\EmployeeFacade;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;

class EmployeeController extends AbstractController
{
    public function __construct(
        private readonly EmployeeFacade $employeeFacade,
    ) {
    }

    #[Route(
        '/api/employee/employee',
        name: 'app_employee_create_employee',
        methods: ['POST']
    )]
    public function createEmployee(): JsonResponse
    {
        $employeeUuid = (string) Uuid::v4();
        $this->employeeFacade->createEmployee(new CreateEmployeeCommand(
            $employeeUuid,
        ));

        return $this->json([
            'employee_uuid' => $employeeUuid,
        ], Response::HTTP_CREATED);
    }
}
