<?php

declare(strict_types=1);

namespace App\Employee\Infrastructure\Repository;

use App\Employee\Domain\Employee;
use App\Employee\Domain\EmployeeRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

class DoctrineEmployeeRepository implements EmployeeRepositoryInterface
{
    public function __construct(
        private readonly EntityManagerInterface $em
    ) {
    }

    public function add(Employee $employee): void
    {
        $this->em->persist($employee);
        $this->em->flush();
    }

    public function find(string $employeeUuid): ?Employee
    {
        return $this->em->getRepository(Employee::class)->find($employeeUuid);
    }
}
