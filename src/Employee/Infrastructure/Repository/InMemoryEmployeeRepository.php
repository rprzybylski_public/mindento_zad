<?php

declare(strict_types=1);

namespace App\Employee\Infrastructure\Repository;

use App\Employee\Domain\Employee;
use App\Employee\Domain\EmployeeRepositoryInterface;

class InMemoryEmployeeRepository implements EmployeeRepositoryInterface
{
    private array $employees = [];

    public function add(Employee $employee): void
    {
        $this->employees[(string) $employee->getEmployeeUuid()] = $employee;
    }

    public function find(string $employeeUuid): ?Employee
    {
        return $this->employees[$employeeUuid];
    }
}
