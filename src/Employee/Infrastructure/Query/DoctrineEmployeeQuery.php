<?php

declare(strict_types=1);

namespace App\Employee\Infrastructure\Query;

use App\Employee\Application\Query\EmployeeQueryInterface;
use App\Employee\Application\Query\EmployeeView;
use Doctrine\DBAL\Connection;
use Symfony\Component\Uid\Uuid;

class DoctrineEmployeeQuery implements EmployeeQueryInterface
{
    public const TABLE_NAME = 'employee__employee';

    public function __construct(
        private readonly Connection $connection
    ) {
    }

    public function findEmployee(string $employeeUuid): ?EmployeeView
    {
        $queryBuilder = $this->connection->createQueryBuilder()
            ->select('e.*')
            ->from(self::TABLE_NAME, 'e')
            ->where('e.employee_uuid = :employee_uuid')
            ->setParameter('employee_uuid', Uuid::fromString($employeeUuid))
        ;

        $data = $this->connection->fetchAssociative($queryBuilder->getSQL(), $queryBuilder->getParameters());

        if (false === $data) {
            return null;
        }

        return EmployeeView::deserialize($data);
    }
}
