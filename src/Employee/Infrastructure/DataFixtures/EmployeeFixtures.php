<?php

declare(strict_types=1);

namespace App\Employee\Infrastructure\DataFixtures;

use App\Employee\Application\Command\CreateEmployeeCommand;
use App\Employee\Application\EmployeeFacade;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Uid\Uuid;

class EmployeeFixtures extends Fixture implements FixtureGroupInterface
{
    public function __construct(
        private readonly EmployeeFacade $employeeFacade
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        $this->createOneEmployee(
            Uuid::fromString('11111111-1111-1111-1111-111111111111'),
        );
    }

    private function createOneEmployee(
        Uuid $employeeUuid
    ): void {
        $this->employeeFacade->createEmployee(new CreateEmployeeCommand(
            (string) $employeeUuid
        ));
    }

    public static function getGroups(): array
    {
        return ['dev'];
    }
}
